#include <ESP8266WebServer.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_I2CDevice.h>

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

// Declaration of BME280 sensor
Adafruit_BME280 bme;

float temperature, humidity, pressure;
int timer = 30;
int nextGet;

void get_sensor_data();

void setup() {
  Serial.begin(115200);

  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;);
  }

  bme.begin(0x76);

  get_sensor_data();
  nextGet = timer;
}

void loop() {
  display.clearDisplay();

  if (nextGet > 0) {
    nextGet -= 1;
  } else {
    get_sensor_data();
    nextGet = timer;
  }

  display.setTextSize(1);
  display.setTextColor(WHITE);

  display.setCursor(0, 0);
  display.println("Temperature:");
  display.setCursor(0, 8);
  display.println("Humidity:");
  display.setCursor(0, 16);
  display.println("Pressure:");
  display.setCursor(0, 32);
  display.println("Next update in:");

  display.setCursor(75, 0);
  display.println(temperature);
  display.setCursor(75, 8);
  display.println(humidity);
  display.setCursor(75, 16);
  display.println(pressure);
  display.setCursor(90, 32);
  display.println(nextGet);
  display.display();

  delay(1000);
}

void get_sensor_data() {
  temperature = bme.readTemperature();
  humidity = bme.readHumidity();
  pressure = bme.readPressure() / 100.0F;
}
