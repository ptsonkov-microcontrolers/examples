// https://randomnerdtutorials.com/esp8266-nodemcu-date-time-ntp-client-server-arduino/
// http://www.cplusplus.com/reference/ctime/tm/
#include <ESP8266WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

// Set WiFi name and password
const char *ssid = "ssidName";
const char *password = "ssidPass";

// Define NTP Client
WiFiUDP ntpUDP;
const char *ntpServer = "pool.ntp.org";
NTPClient timeClient(ntpUDP, ntpServer);
// Set offset time in seconds to adjust for your timezone, for example:
// GMT +1 = 3600
// GMT +8 = 28800
// GMT -1 = -3600
// GMT 0 = 0s
int timeZome = +2;

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.print("connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Initialize a NTPClient to get time
  timeClient.begin();
  int tz = (timeZome * 60) * 60;
  timeClient.setTimeOffset(tz);
  Serial.println();
}

void loop() {

  timeClient.update();

  unsigned long epochTime = timeClient.getEpochTime();
  Serial.print("Epoch Time: ");
  Serial.println(epochTime);

  String formattedTime = timeClient.getFormattedTime();
  Serial.print("Formatted Time: ");
  Serial.println(formattedTime);

  // Create time structure
  struct tm *ptm;
  ptm = gmtime((time_t *)&epochTime);

  // Member    Type    Meaning	Range
  // tm_sec	  int     seconds after the minute	0-61*
  // tm_min    int     minutes after the hour	0-59
  // tm_hour   int     hours since midnight	0-23
  // tm_mday   int     day of the month	1-31
  // tm_mon    int     months since January	0-11
  // tm_year   int     years since 1900
  // tm_wday   int     days since Sunday	0-6
  // tm_yday   int     days since January 1	0-365
  // tm_isdst  int     Daylight Saving Time flag

  String year, month, day;

  int y = ptm->tm_year + 1900;
  int m = ptm->tm_mon + 1;
  int d = ptm->tm_mday;
  Serial.print("date: ");

  year = String(y);

  if (m < 10) {
    month = "0" + String(m);
  } else {
    month = String(m);
  }

  if (d < 10) {
    day = "0" + String(d);
  } else {
    day = String(d);
  }

  String timestamp = year + month + day;
  Serial.println(timestamp);

  delay(1000);
}
