// IMPORTANT!
// Issue with NodeMCU Amica ESP8266
// Put 220Ohm resistor between SD0 and 3.3V
// Ticket:
// https://github.com/esp8266/Arduino/issues/6007
// Solution post:
// https://github.com/esp8266/Arduino/issues/6007?fbclid=IwAR2v8N0nCucgX1-FA_qhKArZX1M6wfEcLXLPSOrW7aIuKzB4kHJGVJ0DCyI#issuecomment-731670661

#include <Arduino.h>
#include <ESP8266WiFi.h>

void setup() {
  Serial.begin(115200);
  while (!Serial) {
  }
  delay(5000);
  Serial.println("Started.");
  delay(5000);
  Serial.println("Going to sleep.");
  ESP.deepSleep(10e6);
}

void loop() {
  Serial.println("This should never happens");
  delay(1000);
}
