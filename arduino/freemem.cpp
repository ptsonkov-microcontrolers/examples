#include <Arduino.h>
#include <MemoryFree.h>

void setup() {
    Serial.begin(9600);
}


void loop() {
    //Serial.println(str);

    Serial.print(F("Free RAM = "));
    Serial.println(freeMemory(), DEC);

    delay(1000);
}
