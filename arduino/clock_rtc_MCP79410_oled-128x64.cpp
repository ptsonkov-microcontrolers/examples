#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <NavBoard.h>

//==============================================================================
// Define I2C communication addresses
//==============================================================================
#define CLOCK_EEPROM_ADDR 0x57
#define CLOCK_RTCC_ADDR 0x6f
#define DISPLAY_ADDR 0x3C

//==============================================================================
// Initialize OLED display
//==============================================================================
int screenWidth = 128;
int screenHeight = 64;
Adafruit_SSD1306 display(screenWidth, screenHeight, &Wire, -1);

//==============================================================================
// Initialize navigation board
//==============================================================================
byte analogPin = A0;
NavBoard board(analogPin, 88, 121, 152, 183, 210, 240);

// RTC valid bits
int vbitSecond = 7;
int vbitMinute = 7;
int vbitHour = 6;
int vbitDate = 6;
int vbitMonth = 5;
int vbitYear = 8;

// Forward function declaration
byte readByte(byte devaddr, byte memaddr, byte read_bytes);
void clockReset();
void printClock();
void printDate();
void menuMovementLogic();
void mainMenu();
void moveMenuCursor(int rows);
void callSetup(String type);
void readClock();
void readDate();
void setupClock(int pos, int row);
void setupDate(int pos, int row);


// Global variales setup
//
// Display meny marker
int displayMenu = 0;
// Selection cursor positions (horizontal or vertical)
int horCursor = 0;
int verCursor = 0;
// Delay preventing multiple clicks
int menuMoveDelay = 100;
// Delay between value change (clock, date)
int changeDelay = 250;
// Create empty values for time/date elements
int second, hour, minute, date, month, year;

// Initial clock (if it was never set)
byte clockCheck;
// minute, hour
int initClock[] = {0, 0};
// date, month, year
int initDate[] = {1, 1, 0};

// Declare reset func (software board reset)
void (* resetFunc) (void) = 0;

//==============================================================================
//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
void setup() {
	// Initializes serial console for debugging
	Serial.begin(115200);
	// Initialize display
	if(!display.begin(SSD1306_SWITCHCAPVCC, DISPLAY_ADDR)) {
		Serial.println(F("SSD1306 allocation failed"));
		for(;;);
	}
	// Initialize for I2C communication
	// Wire.begin();
	// Initialize to listen for button click
	pinMode(A0, INPUT_PULLUP);

	clockCheck = readByte(CLOCK_RTCC_ADDR, 0, 1);

	display.setTextSize(1);
	display.setTextColor(WHITE);
}

void loop(){
	if (clockCheck < 0x80) {
		clockReset();
	} else {
		// int btn = analogRead(A0);
		// Serial.println(btn);
		// delay(250);
		if (displayMenu == 0) {
			display.clearDisplay();
			display.setCursor(0, 0);
			display.print("=====================");
			display.setCursor(40, 10);
			printClock();
			display.setCursor(34, 20);
			printDate();
			display.setCursor(0, 55);
			display.print("=====================");
			display.display();
			menuMovementLogic();
		} else if (displayMenu == 1) {
			display.clearDisplay();
			mainMenu();
			display.display();
			menuMovementLogic();
			moveMenuCursor(3);
		} else if (displayMenu == 11) {
			display.clearDisplay();
			callSetup("clock");
			display.display();
			menuMovementLogic();
		} else if (displayMenu == 12) {
			display.clearDisplay();
			callSetup("date");
			display.display();
			menuMovementLogic();
		} else if (displayMenu == 13) {
			clockReset();
		}
	}
}
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//==============================================================================

//==============================================================================
// General functions
//==============================================================================

// Redraw screen on button click (menu navigation)
void redraw() {
	display.clearDisplay();
	delay(menuMoveDelay);
}

// Convert decimal hour to binary coded hour
uint8_t dec2hex(uint8_t num) {
	uint8_t units = num % 10;
	uint8_t tens = num / 10;
	return (tens << 4) | units;
}

// Convert binary coded hour to decimal hour
uint8_t hex2dec(uint8_t num) {
	uint8_t units = num & 0x0F;
	uint8_t tens = num >> 4;
	return tens*10 + units;
}

// Convert binary coded hour to decimal hour
uint8_t rtc2print(uint8_t num, int validBits) {
	return num & 0xff >> (8-validBits);
}

// Print hours in setupClock()
void printZero(uint8_t tData) {
	if (tData < 10) {
		display.print("0");
		display.print(tData);
	} else {
		display.print(tData);
	}
}

// Convert integer to bitmask
byte bitmask(int validBits) {
	byte mask = 0b0;
	for (int i = 1; i <= validBits; i++) {
		mask = (mask << 1) + 1;
	}
	return mask;
}

//==============================================================================
// Memory Read/Write operations
//==============================================================================

// Read data from memory (RTCC or EEPROM)
byte readByte(byte devaddr, byte memaddr, byte read_bytes) {
	byte data;
	// Connect to I2C device at it's address
	Wire.beginTransmission(devaddr);
	// set to read starting from byte X, i.e. like "move cursor to..."
	Wire.write(memaddr);
	// Close connection to I2C device
	Wire.endTransmission();
	// Ast to read from device with ADDR and return X bytes
	Wire.requestFrom(devaddr, read_bytes);
	// Check if there are available bytes for reading "Wire.available()"
	// and read it "Wire.read()"
	while (Wire.available()) {
		data = Wire.read();
	}
	return data;
}

// Write data to memory (RTCC or EEPROM)
void writeByte(byte devaddr, byte memaddr, byte data) {
	Wire.beginTransmission(devaddr);
	Wire.write(memaddr);
	Wire.write(data);
	Wire.endTransmission();
}

// Write clock to RTCC
void setClock(int arr[]) {
	writeByte(CLOCK_RTCC_ADDR, 0, 0);                //STOP RTC (set byte to 0)
	writeByte(CLOCK_RTCC_ADDR, 1, dec2hex(arr[0]));  //set MINUTE
	writeByte(CLOCK_RTCC_ADDR, 2, dec2hex(arr[1]));  //set HOUR
	writeByte(CLOCK_RTCC_ADDR, 3, 0x09);             //set VBAT (to use backup battery)
	writeByte(CLOCK_RTCC_ADDR, 0, 0x80);             //START RTC, SECOND=00 (set byte to 128)
	delay(100);
}

// Write date to RTCC
void setDate(int arr[]) {
	writeByte(CLOCK_RTCC_ADDR, 4, dec2hex(arr[0]));  //set DATE
	writeByte(CLOCK_RTCC_ADDR, 5, dec2hex(arr[1]));  //set MONTH
	writeByte(CLOCK_RTCC_ADDR, 6, dec2hex(arr[2]));  //set YEAR
	delay(100);
}

//==============================================================================
// Print time and date
//==============================================================================

// Print clock from RTC
void printClock() {
	byte rawByte;

	// Prepare variables
	rawByte = readByte(CLOCK_RTCC_ADDR, 0, 1);
	second = rtc2print(rawByte, vbitSecond);
	rawByte = readByte(CLOCK_RTCC_ADDR, 1, 1);
	minute = rtc2print(rawByte, vbitMinute);
	rawByte = readByte(CLOCK_RTCC_ADDR, 2, 1);
	hour = rtc2print(rawByte, vbitHour);

	// Print request
	if (hour < 10) {
		display.print("0");
	}
	display.print(hour, HEX);
	display.print(":");
	if (minute < 10) {
		display.print("0");
	}
	display.print(minute, HEX);
	display.print(":");
	if (second < 10) {
		display.print("0");
	}
	display.print(second, HEX);
}

// Print date from RTC
void printDate() {
	byte rawByte;

	// Prepare variables
	rawByte = readByte(CLOCK_RTCC_ADDR, 4, 1);
	date = rtc2print(rawByte, vbitDate);
	rawByte = readByte(CLOCK_RTCC_ADDR, 5, 1);
	month = rtc2print(rawByte, vbitMonth);
	rawByte = readByte(CLOCK_RTCC_ADDR, 6, 1);
	year = rtc2print(rawByte, vbitYear);

	// Print request
	if (date < 10) {
		display.print("0");
	}
	display.print(date, HEX);
	display.print(".");
	if (month < 10) {
		display.print("0");
	}
	display.print(month, HEX);
	display.print(".");
	display.print("20");
	if (year < 10) {
		display.print("0");
	}
	display.print(year, HEX);
}

//==============================================================================
// Menu, entries and cursors movement and logic
//==============================================================================

// Menu movement logic
void menuMovementLogic() {
	int button = board.setButton();
	if (displayMenu == 0 && (button == 3 || button == 4)) {
		displayMenu = 1;
		redraw();
	} else if (displayMenu == 1 && button == 1) {
		// Exit to main screen
		displayMenu = 0;
		redraw();
	} else if (displayMenu == 1 && verCursor == 0 && button == 6) {
		// Enter clock setup
		displayMenu = 11;
		readClock();
		redraw();
	} else if (displayMenu == 11 && button == 1) {
		// Exit clock setup (cancel)
		displayMenu = 1;
		redraw();
	} else if (displayMenu == 11 && button == 6) {
		// Exit clock setup (saved)
		displayMenu = 0;
		int data[] = {minute, hour};
		setClock(data);
		redraw();
	} else if (displayMenu == 1 && verCursor == 10 && button == 6) {
		// Enter date setup
		displayMenu = 12;
		readDate();
		redraw();
	} else if (displayMenu == 12 && button == 1) {
		// Exit date setup (cancel)
		displayMenu = 1;
		redraw();
	} else if (displayMenu == 12 && button == 6) {
		// Exit date setup (saved)
		displayMenu = 0;
		int data[] = {date, month, year};
		setDate(data);
		redraw();
	} else if (displayMenu == 1 && verCursor == 20 && button == 6) {
		// Enter clock reset
		displayMenu = 13;
		redraw();
	}

	// Set cursor to 0 when move between menus
	if (button == 1 || button == 6) {
		verCursor = 0;
	}
}

// Menu cursor movement
void moveMenuCursor(int rows) {
	int button = board.setButton();
	if (button == 4 && verCursor < ((rows * 10) - 10)) {
		verCursor += 10;
		redraw();
	} else if (button == 3 && verCursor > 0) {
		verCursor -= 10;
		redraw();
	}
}

// Main menu screen
void mainMenu() {
	display.setCursor(0, verCursor);
	display.write(16);
	// option1
	display.setCursor(10, 0);
	display.print("Clock setup");
	display.setCursor(110, 0);
	display.write(24);
	// option2
	display.setCursor(10, 10);
	display.print("Date setup");
	// option3
	display.setCursor(10, 20);
	display.print("Clock reset");
	display.setCursor(110, 20);
	display.write(25);
	// Command row
	display.setCursor(0, 55);
	display.write(60);
	display.print(" BACK");
	display.setCursor(80, 55);
	display.print("SELECT ");
	display.write(62);
}

void callSetup(String type) {
	display.setCursor(0, 0);
	// Call setup command
	if (type == "clock") {
		display.print("==== Clock setup ====");
		setupClock(40, 15);
	} else if (type == "date") {
		display.print("==== Date setup =====");
		setupDate(30, 15);
	}
	// Command row
	display.setCursor(0, 55);
	display.write(60);
	display.print(" CANCEL");
	display.setCursor(90, 55);
	display.print("SAVE ");
	display.write(62);
}

void clockReset() {
	display.clearDisplay();
	display.setCursor(0, 0);
	display.print("====================");
	display.setCursor(22, 15);
	display.print("Reset clock...");
	display.setCursor(0, 55);
	display.print("=====================");
	display.display();
	delay(3000);
	setClock(initClock);
	setDate(initDate);
	resetFunc();
}

//==============================================================================
// Clock setup
//==============================================================================

// Read clock  from RTC
void readClock() {
	byte rawByte;

	// Prepare variables
	rawByte = readByte(CLOCK_RTCC_ADDR, 1, 1);
	minute = hex2dec(rawByte & bitmask(vbitMinute));
	rawByte = readByte(CLOCK_RTCC_ADDR, 2, 1);
	hour = hex2dec(rawByte & bitmask(vbitHour));
}

// Navigate and set clock
void adjustClock() {
	int button = board.setButton();
	if (horCursor == 0) {
		if (button == 3) {
			if (hour == 23) {
				hour = 0;
			} else {
				hour = hour + 1;
			}
			delay(changeDelay);
		} else if (button == 4) {
			if (hour == 0) {
				hour = 23;
			} else {
				hour = hour - 1;
			}
			delay(changeDelay);
		}
	} else if (horCursor == 1) {
		if (button == 3) {
			if (minute == 59) {
				minute = 0;
			} else {
				minute = minute + 1;
			}
			delay(changeDelay);
		} else if (button == 4) {
			if (minute == 0) {
				minute = 59;
			} else {
				minute = minute - 1;
			}
			delay(changeDelay);
		}
	}

	if (button == 2) {
		horCursor = 0;
		redraw();
	} else if (button == 5) {
		horCursor = 1;
		redraw();
	}
}

// Clock setup screen
void setupClock(int pos, int row) {
	// Manipulate clock
	adjustClock();

	int valPos = pos;

	// Print hours
	display.setCursor(valPos, row);
	printZero(hour);
	// Print separator
	valPos = valPos + 12;
	display.setCursor(valPos, row);
	display.print(":");
	// Print minutes
	valPos = valPos + 6;
	display.setCursor(valPos, row);
	printZero(minute);
	// Print separator
	valPos = valPos + 12;
	display.setCursor(valPos, row);
	display.print(":");
	// Print seconds
	valPos = valPos + 6;
	display.setCursor(valPos, row);
	display.print("00");

	// Print selection cursor
	if (horCursor == 0) {
		display.setCursor(pos, (row + 10));
		display.print("^^");
	} else if (horCursor == 1) {
		display.setCursor((pos + 18), (row + 10));
		display.print("^^");
	}

	// Print arrows
	display.setCursor(100, row);
	display.write(27);
	display.write(18);
	display.write(26);
}

//==============================================================================
// Date setup
//==============================================================================

// Read date from RTC
void readDate() {
	byte rawByte;

	// Prepare variables
	rawByte = readByte(CLOCK_RTCC_ADDR, 4, 1);
	date = hex2dec(rawByte & bitmask(vbitDate));
	rawByte = readByte(CLOCK_RTCC_ADDR, 5, 1);
	month = hex2dec(rawByte & bitmask(vbitMonth));
	rawByte = readByte(CLOCK_RTCC_ADDR, 6, 1);
	year = hex2dec(rawByte & bitmask(vbitYear));
}

// Navigate and set date
void adjustDate() {
	int button = board.setButton();
	if (horCursor == 0) {
		if (button == 3) {
			if (date == 31) {
				date = 1;
			} else {
				date = date + 1;
			}
			delay(changeDelay);
		} else if (button == 4) {
			if (date == 1) {
				date = 31;
			} else {
				date = date - 1;
			}
			delay(changeDelay);
		}
	} else if (horCursor == 1) {
		if (button == 3) {
			if (month == 12) {
				month = 1;
			} else {
				month = month + 1;
			}
			delay(changeDelay);
		} else if (button == 4) {
			if (month == 1) {
				month = 12;
			} else {
				month = month - 1;
			}
			delay(changeDelay);
		}
	} else if (horCursor == 2) {
		if (button == 3) {
			if (year == 99) {
				year = 0;
			} else {
				year = year + 1;
			}
			delay(changeDelay);
		} else if (button == 4) {
			if (year == 0) {
				year = 99;
			} else {
				year = year - 1;
			}
			delay(changeDelay);
		}
	}

	if ((horCursor == 0) && (button == 5)) {
		horCursor = 1;
		redraw();
	} else if ((horCursor == 1) && (button == 5)) {
		horCursor = 2;
		redraw();
	} else if ((horCursor == 2) && (button == 2)) {
		horCursor = 1;
		redraw();
	} else if ((horCursor == 1) && (button == 2)) {
		horCursor = 0;
		redraw();
	}
}

// Date setup screen
void setupDate(int pos, int row) {
	// Manipulate date
	adjustDate();

	int valPos = pos;

	// Print date
	display.setCursor(valPos, row);
	printZero(date);
	// Print separator
	valPos = valPos + 12;
	display.setCursor(valPos, row);
	display.print(".");
	// Print month
	valPos = valPos + 6;
	display.setCursor(valPos, row);
	printZero(month);
	// Print separator
	valPos = valPos + 12;
	display.setCursor(valPos, row);
	display.print(".");
	// Print year
	valPos = valPos + 6;
	display.setCursor(valPos, row);
	display.print("20");
	valPos = valPos + 12;
	display.setCursor(valPos, row);
	printZero(year);

	// Print selection cursor
	if (horCursor == 0) {
		display.setCursor(pos, (row + 10));
		display.print("^^");
	} else if (horCursor == 1) {
		display.setCursor((pos + 18), (row + 10));
		display.print("^^");
	} else if (horCursor == 2) {
		display.setCursor((pos + 48), (row + 10));
		display.print("^^");
	}

	// Print arrows
	display.setCursor(100, row);
	display.write(27);
	display.write(18);
	display.write(26);
}
