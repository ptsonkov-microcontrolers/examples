#include <Arduino.h>
#include <LiquidCrystal.h>
#include <Servo.h>

// Initialize display
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

// Initialize servo
Servo myservo;

// Define start timer
int hour = 0;
int minute = 0;
int second = 0;

// Set clock loop speed and control timers
// !!! loopDelay * ctBorder = 1000 !!!
int ct = 0; // Control timer, NOT printed!
int loopDelay = 1000;
int ctBorder = 1;

// Define command uttons
const int hs = A0;
const int ms = A1;
int state1 = 0;
int state2 = 0;

// Servo settings
int servoPosition = 0;
int servoSpeed = 20;
int servoDegrees = 180;
int servoStep = 1;

void greeting(int period)
{
  // Define time borders
  int morning = 6;
  int noon = 12;
  int evening = 18;
  int night = 22;

  // Set greetings
  if (hour >= morning && hour < noon) {
    lcd.print("MORNING");
  }
  if (hour >= noon && hour < evening) {
    lcd.print("AFTERNOON");
  }
  if (hour >= evening && hour < night) {
    lcd.print("EVENING");
  }
  if (hour >= night || hour < morning) {
    lcd.print("NIGHT");
  }
}

int servo() {

  int fMiliseconds;
  int fSeconfds;

  for (servoPosition = 0; servoPosition <= servoDegrees; servoPosition += servoStep) {
    myservo.write(servoPosition);
    delay(servoSpeed);
  }
  for (servoPosition = servoDegrees; servoPosition >= 0; servoPosition -= servoStep) {
    myservo.write(servoPosition);
    delay(servoSpeed);
  }

  fMiliseconds = 180*2*servoSpeed;
  fSeconfds = fMiliseconds/1000;
  return round(fSeconfds);
}

void setup()
{
  lcd.begin(20, 4);
  pinMode(hs, INPUT_PULLUP);
  pinMode(ms, INPUT_PULLUP);
  // attaches the servo on pin 9 to the servo object
  myservo.attach(9);
}

void loop()
{
  // Print current time
  lcd.setCursor(0, 0);
  lcd.print("TIME:   " );
  if(hour < 10) {
    lcd.print(0);
  }
  lcd.print(hour);
  lcd.print(":");
  if(minute < 10) {
    lcd.print(0);
  }
  lcd.print(minute);
  lcd.print(":");
  if(second < 10) {
    lcd.print(0);
  }
  lcd.print(second);

  // Print daily greetings
  lcd.setCursor(0, 1);
  lcd.print("PERIOD: " );
  greeting(hour);

  // Set next second => minute => hour
  ct = ct + 1;
  if (ct == ctBorder) {
    ct = 0;
    second = second + 1;
  }
  if (second == 60) {
    second = 0;
    minute = minute + 1;
  }
  if (minute == 60) {
    minute = 0;
    hour = hour + 1;
  }
  if (hour == 24) {
    hour = 0;
  }

  // Setting hours
  state1 = digitalRead(hs);
  if (state1 == 0)
  {
    second = 0;
    hour = hour + 1;
    if (hour == 24) {
      hour = 0;
    }
  }

  // Setting minutes
  state2 = digitalRead(ms);
  if (state2 == 0) {
    second = 0;
    minute = minute + 1;
  }

  if (second == 10 ||second == 40 ) {
    int servoSec;
    lcd.setCursor(0, 2);
    lcd.print("Feeding the cat...");
    servoSec = servo();
    // lcd.setCursor(0, 3);
    // lcd.print(servoSec);
    second = second + servoSec;
  }

  // Clear display for next loop
  delay(loopDelay);
  lcd.clear();
}
