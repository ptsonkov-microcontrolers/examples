#include <Arduino.h>

// Based on https://forum.arduino.cc/index.php?topic=8558.0
//==============================================================================
//
// Ground---1K---|--------|--------|-------|-------|-------|---Analog pin X
//               |        |        |       |       |       |
//              btn1     btn2     btn3    btn4    btn5    btn6
//               |        |        |       |       |       |
//            220 Ohm  390 Ohm  680 Ohm   2.2K    4.7K    7.4K
//     +5V-------|--------|--------|-------|-------|-------|
//
//==============================================================================

//two dimensional array for asigning the buttons (buttonID, LOW, HIGH)
int Button[22][3] = {
	{0, 0, 0}, // button released
	{1, 316, 317}, // button 1
	{2, 203, 204}, // button 2
	{3, 129, 130}, // button 3
	{4, 46, 48}, // button 4
	{5, 18, 19}, // button 5
	{6, 11, 12}, // button 6
	{7, 421, 421}, // button 1 + button 2
	{8, 382, 382}, // button 1 + button 3
	{9, 340, 341}, // button 1 + button 4
	{10, 326, 327}, // button 1 + button 5
	{11, 323, 323}, // button 1 + button 6
	{12, 290, 291}, // button 2 + button 3
	{13, 235, 237}, // button 2 + button 4
	{14, 217, 218}, // button 2 + button 5
	{15, 212, 213}, // button 2 + button 6
	{16, 167, 168}, // button 3 + button 4
	{17, 146, 147}, // button 3 + button 5
	{18, 140, 141}, // button 3 + button 6
	{19, 66, 67}, // button 4 + button 5
	{20, 58, 60}, // button 4 + button 6
	{21, 31, 32} // button 5 + button 6
};

int newButtonValue;
int analogPin = A5;
int currentButtonID = 0;              // Initial button state (0 mean unpressed)
int reactionCounter = 0;              // Wait counter before to announce state change
int waitToReact = 1000;      			    // Wait time (in miliseconds) before reaction

void ButtonLogic();
int ButtonCheck(int aData);
void Action(int btnID);
void serialDebug(byte aPin, int timer);

void setup() {

	Serial.begin(9600);

}

void loop() {

	// serialDebug(analogPin, 250);

	ButtonLogic();
}

void ButtonLogic() {

	// Read analog input value
	int buttonValue = analogRead(analogPin);
	// Assign new button ID
	int buttonID = ButtonCheck(buttonValue);
	// Check if there is change in button ID since last check
	if (buttonID != currentButtonID) {
		reactionCounter++;
		if (reactionCounter == waitToReact) {
			currentButtonID = buttonID;
			Action(currentButtonID);
			reactionCounter = 0;
		}
	} else {
		reactionCounter = 0;
	}
}

int ButtonCheck(int aData) {
	// Loop trough value array
	for(int i = 0; i < 22; i++) {
		// Check which values match to analog value
		if((aData >= Button[i][1]) && (aData <= Button[i][2])) {
			int btnID = Button[i][0];
			return btnID;
		}
	}
}

void Action(int btnID) {

	if(btnID == 0) {
		Serial.println(F("Button released"));
	} else if(btnID == 1) {
		Serial.println(F("Button1"));
	} else if(btnID == 2) {
		Serial.println(F("Button2"));
	} else if(btnID == 3) {
		Serial.println(F("Button3"));
	} else if(btnID == 4) {
		Serial.println(F("Button4"));
	} else if(btnID == 5) {
		Serial.println(F("Button5"));
	} else if(btnID == 6) {
		Serial.println(F("Button6"));
	} else if(btnID == 7) {
		Serial.println(F("Button1 + Button2"));
	} else if(btnID == 8) {
		Serial.println(F("Button1 + Button3"));
	} else if(btnID == 9) {
		Serial.println(F("Button1 + Button4"));
	} else if(btnID == 10) {
		Serial.println(F("Button1 + Button5"));
	} else if(btnID == 11) {
		Serial.println(F("Button1 + Button6"));
	} else if(btnID == 12) {
		Serial.println(F("Button2 + Button3"));
	} else if(btnID == 13) {
		Serial.println(F("Button2 + Button4"));
	} else if(btnID == 14) {
		Serial.println(F("Button2 + Button5"));
	} else if(btnID == 15) {
		Serial.println(F("Button2 + Button6"));
	} else if(btnID == 16) {
		Serial.println(F("Button3 + Button4"));
	} else if(btnID == 17) {
		Serial.println(F("Button3 + Button5"));
	} else if(btnID == 18) {
		Serial.println(F("Button3 + Button6"));
	} else if(btnID == 19) {
		Serial.println(F("Button4 + Button5"));
	} else if(btnID == 20) {
		Serial.println(F("Button4 + Button6"));
	} else if(btnID == 21) {
		Serial.println(F("Button5 + Button6"));
	}
}

// Declare debug function
void serialDebug(byte aPin, int timer) {
	int btn = analogRead(aPin);
	Serial.println(btn);
	delay(timer);
}
