#include <Arduino.h>
#include <Wire.h>

// Define I2C communication addresses
#define CLOCK_EEPROM_ADDR 0x57
#define CLOCK_RTCC_ADDR 0x6f

// EEPROM size as per datasheet
int eepromLen = 128;

// Generic variables
byte rawByte;

// Read data from memory (RTCC or EEPROM)
byte readByte(byte devaddr, byte memaddr, byte read_bytes) {
	byte data;
	// Connect to I2C device at it's address
	Wire.beginTransmission(devaddr);
	// set to read starting from byte X, i.e. like "move cursor to..."
	Wire.write(memaddr);
	// Close connection to I2C device
	Wire.endTransmission();
	// Ast to read from device with ADDR and return X bytes
	Wire.requestFrom(devaddr, read_bytes);
	// Check if there are available bytes for reading "Wire.available()"
	// and read it "Wire.read()"
	while (Wire.available()) {
		data = Wire.read();
	}
	return data;
}

void printZero(uint8_t tData) {
	if ((tData >= 10) && (tData < 100)) {
		Serial.print("0");
		Serial.print(tData);
	} else if (tData < 10) {
		Serial.print("00");
		Serial.print(tData);
	} else {
		Serial.print(tData);
	}
}

void setup() {
	// Initializes serial console for debugging
	Serial.begin(9600);
	// Initialize for I2C communication
	Wire.begin();

  for (int i = 0 ; i < eepromLen ; i++) {
    rawByte = readByte(CLOCK_EEPROM_ADDR, i, 1);

    // print byte in different systems
    Serial.println();
    Serial.print("MemAddr: ");
    printZero(i);
    Serial.print(" | ");
    Serial.print("HEX: ");
    Serial.print(rawByte, HEX);
    Serial.print(" | ");
    Serial.print("DEC: ");
    Serial.print(rawByte, DEC);
    Serial.print(" | ");
    Serial.print("BIN: ");
    Serial.print(rawByte, BIN);
  }
}

void loop() {}
