#include <Arduino.h>
#include <LiquidCrystal.h>
/*
   LCD circuit setup:
   * LCD pin1  (VSS)     -  Arduino GND
   * LCD pin2  (VCC)     -  Arduino 5V
   * LCD pin3  (Vo)      -  Potentiometer wiper (display contrast control)
   * LCD pin4  (RS)      -  Arduino Digital-12
   * LCD pin5  (R/W)     -  Arduino GND
   * LCD pin6  (E)       -  Arduino Digital-11
   * LCD pin11 (DB4)     -  Arduino Digital-5
   * LCD pin12 (DB5)     -  Arduino Digital-4
   * LCD pin13 (DB6)     -  Arduino Digital-3
   * LCD pin14 (DB7)     -  Arduino Digital-2
   * LCD pin15 (A)       -  Display LED backlight + (5V)
   * LCD pin16 (K)       -  Display LED backlight - (GND)
   * Potentiometer ends - to 5V and GND
*/

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

byte pipe[] = {
  B00100,
  B00100,
  B00100,
  B00100,
  B00100,
  B00100,
  B00100,
  B00100
};

byte lowerLeft[] = {
  B00100,
  B00100,
  B00100,
  B00100,
  B00111,
  B00000,
  B00000,
  B00000
};

byte lowerRight[] = {
  B00100,
  B00100,
  B00100,
  B00100,
  B11100,
  B00000,
  B00000,
  B00000
};

byte upperLeft[] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B00111,
  B00100,
  B00100,
  B00100
};

byte upperRight[] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B11100,
  B00100,
  B00100,
  B00100
};

byte dash[] = {
  B00000,
  B00000,
  B00000,
  B00000,
  B11111,
  B00000,
  B00000,
  B00000
};

void setup() {
// Create custom characters
  lcd.createChar(1, pipe);
  lcd.createChar(2, lowerLeft);
  lcd.createChar(3, lowerRight);
  lcd.createChar(4, upperLeft);
  lcd.createChar(5, upperRight);
  lcd.createChar(6, dash);
  // lcd.createChar(7, slash2);
  // lcd.createChar(8, down);
  // Define display size
  lcd.begin(20, 4);
  lcd.setCursor(2,0);
  lcd.print("\004\006\006\006\006\006\006\006\006\006\006\006\006\006\006\005");
  lcd.setCursor(2,1);
  lcd.print("\001 \004\006\006\006\006\006\006\006\006\006\006\005 \001");
  lcd.setCursor(2,2);
  lcd.print("\001 \002\006\006\006\006\006\006\006\006\006\006\003 \001");
  lcd.setCursor(2,3);
  lcd.print("\002\006\006\006\006\006\006\006\006\006\006\006\006\006\006\003");
}


void loop()
{
  // lcd.scrollDisplayLeft();
  // delay(150);
}
