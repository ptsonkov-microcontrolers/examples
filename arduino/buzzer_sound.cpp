#include <Arduino.h>
#include "tones.h"

//==============================================================================
// Sound setup
//==============================================================================
// Sound pin
int soundPin = 8;
// Sound speed in miliseconds
int soundSpeed = 1000;
// Pause between notes coefficient. Default +30%
int pauseCoef = 1;

// Melody - tone sequence
int toneSequence[] = {
	_C4, _E4, _G4, _C5
};

// Duration - tone play duration:
// 2 - half note, 4 = quarter note, 8 = eighth note and so on.
int toneDuration[] = {
	2, 2, 2, 1
};

void setup() {

	// iterate over the notes of the toneSequence:
	int melodyLength = (sizeof(toneSequence) / sizeof(toneSequence[0]));
	for (int melodyNote = 0; melodyNote < melodyLength; melodyNote++) {

		// Calculate note duration:
		// (1000 milisec / tempo BitsPerMinute) / tone duration
		int noteDuration = soundSpeed / toneDuration[melodyNote];
		tone(soundPin, toneSequence[melodyNote], noteDuration);

		// to distinguish the notes, set a minimum time between them.
		int pauseBetweenNotes = noteDuration * pauseCoef;
		delay(pauseBetweenNotes);

		// stop the tone playing:
		noTone(soundPin);
	}
}

void loop() {
	// no need to repeat the toneSequence.
}
