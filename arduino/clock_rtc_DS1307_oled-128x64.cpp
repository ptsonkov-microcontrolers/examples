#include <Arduino.h>
#include <Wire.h>
#include <DS1307.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <NavBoard.h>

//==============================================================================
// Set DS1307 RTC clock object
//==============================================================================
DS1307 clock;

//==============================================================================
// Define I2C communication addresses
//==============================================================================
#define CLOCK_RTC_ADDR 0x68
#define DISPLAY_ADDR 0x3C

//==============================================================================
// Initialize OLED display
//==============================================================================
int screenWidth = 128;
int screenHeight = 64;
Adafruit_SSD1306 display(screenWidth, screenHeight, &Wire, -1);

//==============================================================================
// Initialize navigation board
//==============================================================================
byte analogPin = A0;
NavBoard board(analogPin, 88, 121, 152, 183, 210, 240);

// Forward function declaration
byte readByte(byte devaddr, byte memaddr, byte read_bytes);
void clockReset();
void printClock();
void printDate();
void menuMovementLogic();
void mainMenu();
void moveMenuCursor(int rows);
void callSetup(String type);
void setupClock(int pos, int row);
void setupDate(int pos, int row);


// Global variales setup
//
// Display meny marker
int displayMenu = 0;
// Selection cursor positions (horizontal or vertical)
int horCursor = 0;
int verCursor = 0;
// Delay preventing multiple clicks
int menuMoveDelay = 100;
// Delay between value change (clock, date)
int changeDelay = 250;
// Create empty values for time/date elements
int hour, minute, date, month, year;

// Initial clock (if it was never set)
byte clockCheck;

// Declare reset func (software board reset)
void (* resetFunc) (void) = 0;

//==============================================================================
//vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
void setup() {
	// Initializes serial console for debugging
	Serial.begin(9600);
	// Initialize DS1307 RTC
	clock.begin();
	// Initialize display
	if(!display.begin(SSD1306_SWITCHCAPVCC, DISPLAY_ADDR)) {
		Serial.println(F("SSD1306 allocation failed"));
		for(;;);
	}
	// Initialize for I2C communication
	// Wire.begin();
	// Initialize to listen for button click
	pinMode(A0, INPUT_PULLUP);

	clockCheck = readByte(CLOCK_RTC_ADDR, 0, 1);

	display.setTextSize(1);
	display.setTextColor(WHITE);
}

void loop(){
	if (clockCheck >= 0x80) {
		clockReset();
	} else {
		if (displayMenu == 0) {
			display.clearDisplay();
			display.setCursor(0, 0);
			display.print(F("====================="));
			display.setCursor(40, 15);
			printClock();
			display.setCursor(34, 30);
			printDate();
			display.setCursor(0, 55);
			display.print(F("====================="));
			display.display();
			menuMovementLogic();
		} else if (displayMenu == 1) {
			display.clearDisplay();
			mainMenu();
			display.display();
			menuMovementLogic();
			moveMenuCursor(3);
		} else if (displayMenu == 11) {
			display.clearDisplay();
			callSetup("clock");
			display.display();
			menuMovementLogic();
		} else if (displayMenu == 12) {
			display.clearDisplay();
			callSetup("date");
			display.display();
			menuMovementLogic();
		} else if (displayMenu == 13) {
			clockReset();
		}
	}
}
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//==============================================================================

//==============================================================================
// General functions
//==============================================================================

// Redraw screen on button click (menu navigation)
void redraw() {
	display.clearDisplay();
	delay(menuMoveDelay);
}

//==============================================================================
// Memory Read/Write operations
//==============================================================================

// Read data from memory (RTCC or EEPROM)
byte readByte(byte devaddr, byte memaddr, byte read_bytes) {
	byte data;
	// Connect to I2C device at it's address
	Wire.beginTransmission(devaddr);
	// set to read starting from byte X, i.e. like "move cursor to..."
	Wire.write(memaddr);
	// Close connection to I2C device
	Wire.endTransmission();
	// Ast to read from device with ADDR and return X bytes
	Wire.requestFrom(devaddr, read_bytes);
	// Check if there are available bytes for reading "Wire.available()"
	// and read it "Wire.read()"
	while (Wire.available()) {
		data = Wire.read();
	}
	return data;
}

//==============================================================================
// Print time and date
//==============================================================================

void printClock() {
	clock.getTime();
	if (clock.hour < 10) {
		display.print(F("0"));
	}
	display.print(clock.hour, DEC);
	display.print(F(":"));
	if (clock.minute < 10) {
		display.print(F("0"));
	}
	display.print(clock.minute, DEC);
	display.print(F(":"));
	if (clock.second < 10) {
		display.print(F("0"));
	}
	display.print(clock.second, DEC);
}

void printDate() {
	clock.getTime();
	if (clock.dayOfMonth < 10) {
		display.print(F("0"));
	}
	display.print(clock.dayOfMonth, DEC);
	display.print(F("."));
	if (clock.month < 10) {
		display.print(F("0"));
	}
	display.print(clock.month, DEC);
	display.print(F("."));
	display.print(clock.year+2000, DEC);
}

//==============================================================================
// Menu, entries and cursors movement and logic
//==============================================================================

// Menu movement logic
void menuMovementLogic() {
	int button = board.setButton();
	if (displayMenu == 0 && (button == 3 || button == 4)) {
		displayMenu = 1;
		redraw();
	} else if (displayMenu == 1 && button == 1) {
		// Exit to main screen
		displayMenu = 0;
		redraw();
	} else if (displayMenu == 1 && verCursor == 0 && button == 6) {
		// Enter clock setup
		// Exit from menu (save or cancel) come form setup function
		displayMenu = 11;
		clock.getTime();
		hour = clock.hour;
		minute = clock.minute;
		redraw();
	} else if (displayMenu == 1 && verCursor == 10 && button == 6) {
		// Enter date setup
		// Exit from menu (save or cancel) come form setup function
		displayMenu = 12;
		clock.getTime();
		date = clock.dayOfMonth;
		month = clock.month;
		year = clock.year;
		redraw();
	} else if (displayMenu == 1 && verCursor == 20 && button == 6) {
		// Enter clock reset
		displayMenu = 13;
		redraw();
	}

	// Set cursor to 0 when move between menus
	if (button == 1 || button == 6) {
		verCursor = 0;
	}
}

// Menu cursor movement
void moveMenuCursor(int rows) {
	int button = board.setButton();
	if (button == 4 && verCursor < ((rows * 10) - 10)) {
		verCursor += 10;
		redraw();
	} else if (button == 3 && verCursor > 0) {
		verCursor -= 10;
		redraw();
	}
}

// Main menu screen
void mainMenu() {
	display.setCursor(0, verCursor);
	display.write(16);
	// option1
	display.setCursor(10, 0);
	display.print(F("Clock setup"));
	display.setCursor(110, 0);
	display.write(24);
	// option2
	display.setCursor(10, 10);
	display.print(F("Date setup"));
	// option3
	display.setCursor(10, 20);
	display.print(F("Clock reset"));
	display.setCursor(110, 20);
	display.write(25);
	// Command row
	display.setCursor(0, 55);
	display.write(60);
	display.print(F(" BACK"));
	display.setCursor(80, 55);
	display.print(F("SELECT "));
	display.write(62);
}

void callSetup(String type) {
	display.setCursor(0, 0);
	// Call setup command
	if (type == "clock") {
		display.print(F("==== Clock setup ===="));
		setupClock(40, 15);
	} else if (type == "date") {
		display.print(F("==== Date setup ====="));
		setupDate(30, 15);
	}
	// Command row
	display.setCursor(0, 55);
	display.write(60);
	display.print(F(" CANCEL"));
	display.setCursor(90, 55);
	display.print(F("SAVE "));
	display.write(62);
}

void clockReset() {
	display.clearDisplay();
	display.setCursor(0, 0);
	display.print(F("===================="));
	display.setCursor(22, 15);
	display.print(F("Reset clock..."));
	display.setCursor(0, 55);
	display.print(F("====================="));
	display.display();
	delay(3000);
	clock.stopClock();
	clock.fillByYMD(2008,1,29);           //YYYY,MM,DD
	clock.fillByHMS(18,30,00);            //hh:mm:ss
	clock.fillDayOfWeek(TUE);             //MON,TUE,WED,THU,FRI,SAT,SUN
	clock.setTime();
	clock.startClock();
	resetFunc();
}

//==============================================================================
// Clock setup
//==============================================================================

// Clock setup screen
void setupClock(int pos, int row) {

	clock.stopClock();
	int valPos = pos;
	int button = board.setButton();

	if (horCursor == 0) {
		// Increase/Decrease hours
		if (button == 3) {
			if (hour == 23) {
				hour = 0;
			} else {
				hour = hour + 1;
			}
			delay(changeDelay);
		} else if (button == 4) {
			if (hour == 0) {
				hour = 23;
			} else {
				hour = hour - 1;
			}
			delay(changeDelay);
		}
	} else if (horCursor == 1) {
		// Increase/Decrease minutes
		if (button == 3) {
			if (minute == 59) {
				minute = 0;
			} else {
				minute = minute + 1;
			}
			delay(changeDelay);
		} else if (button == 4) {
			if (minute == 0) {
				minute = 59;
			} else {
				minute = minute - 1;
			}
			delay(changeDelay);
		}
	}

	if (button == 2) {
		// Move cursor to hours
		horCursor = 0;
		redraw();
	} else if (button == 5) {
		// Move cursor to minutes
		horCursor = 1;
		redraw();
	} else if (button == 6) {
		// Exit clock setup (Save to RTC)
		displayMenu = 0;
		clock.fillByHMS(hour,minute,0);
		clock.setTime();
		clock.startClock();
		redraw();
	} else if (button == 1) {
		// Exit clock setup (Cancel)
		displayMenu = 1;
		redraw();
	}

	// Print hours
	display.setCursor(valPos, row);
	if (hour < 10) {
		display.print(F("0"));
	}
	display.print(hour, DEC);
	// Print separator
	valPos = valPos + 12;
	display.setCursor(valPos, row);
	display.print(F(":"));
	// Print minutes
	valPos = valPos + 6;
	display.setCursor(valPos, row);
	if (minute < 10) {
		display.print(F("0"));
	}
	display.print(minute, DEC);
	// Print separator
	valPos = valPos + 12;
	display.setCursor(valPos, row);
	display.print(F(":"));
	// Print seconds
	valPos = valPos + 6;
	display.setCursor(valPos, row);
	display.print(F("00"));

	// Print selection cursor
	if (horCursor == 0) {
		display.setCursor(pos, (row + 10));
		display.print(F("^^"));
	} else if (horCursor == 1) {
		display.setCursor((pos + 18), (row + 10));
		display.print(F("^^"));
	}

	// Print arrows
	display.setCursor(100, row);
	display.write(27);
	display.write(18);
	display.write(26);
}

//==============================================================================
// Date setup
//==============================================================================

// Date setup screen
void setupDate(int pos, int row) {

	clock.stopClock();
	int valPos = pos;
	int button = board.setButton();

	if (horCursor == 0) {
		if (button == 3) {
			if (date == 31) {
				date = 1;
			} else {
				date = date + 1;
			}
			delay(changeDelay);
		} else if (button == 4) {
			if (date == 1) {
				date = 31;
			} else {
				date = date - 1;
			}
			delay(changeDelay);
		}
	} else if (horCursor == 1) {
		if (button == 3) {
			if (month == 12) {
				month = 1;
			} else {
				month = month + 1;
			}
			delay(changeDelay);
		} else if (button == 4) {
			if (month == 1) {
				month = 12;
			} else {
				month = month - 1;
			}
			delay(changeDelay);
		}
	} else if (horCursor == 2) {
		if (button == 3) {
			if (year == 99) {
				year = 0;
			} else {
				year = year + 1;
			}
			delay(changeDelay);
		} else if (button == 4) {
			if (year == 0) {
				year = 99;
			} else {
				year = year - 1;
			}
			delay(changeDelay);
		}
	}

	if ((horCursor == 0) && (button == 5)) {
		horCursor = 1;
		redraw();
	} else if ((horCursor == 1) && (button == 5)) {
		horCursor = 2;
		redraw();
	} else if ((horCursor == 2) && (button == 2)) {
		horCursor = 1;
		redraw();
	} else if ((horCursor == 1) && (button == 2)) {
		horCursor = 0;
		redraw();
	} else if (button == 6) {
		// Exit date setup (Save to RTC)
		displayMenu = 0;
		clock.fillByYMD(year+2000,month,date);
		clock.setTime();
		clock.startClock();
		redraw();
	} else if (button == 1) {
		// Exit date setup (Cancel)
		displayMenu = 1;
		redraw();
	}


	// Print date
	display.setCursor(valPos, row);
	if (date < 10) {
		display.print(F("0"));
	}
	display.print(date, DEC);
	// Print separator
	valPos = valPos + 12;
	display.setCursor(valPos, row);
	display.print(F("."));
	// Print month
	valPos = valPos + 6;
	display.setCursor(valPos, row);
	if (month < 10) {
		display.print(F("0"));
	}
	display.print(month, DEC);
	// Print separator
	valPos = valPos + 12;
	display.setCursor(valPos, row);
	display.print(F("."));
	// Print year
	valPos = valPos + 6;
	display.setCursor(valPos, row);
	display.setCursor(valPos, row);
	display.print(year+2000, DEC);

	// Print selection cursor
	if (horCursor == 0) {
		display.setCursor(pos, (row + 10));
		display.print(F("^^"));
	} else if (horCursor == 1) {
		display.setCursor((pos + 18), (row + 10));
		display.print(F("^^"));
	} else if (horCursor == 2) {
		display.setCursor((pos + 48), (row + 10));
		display.print(F("^^"));
	}

	// Print arrows
	display.setCursor(100, row);
	display.write(27);
	display.write(18);
	display.write(26);
}
