#include <Arduino.h>

int redPin = 2;
int yellowPin = 3;
int greenPin = 4;

void setup() {
  pinMode(redPin, OUTPUT);
  pinMode(yellowPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
}

void loop() {
  digitalWrite(redPin, HIGH);
  delay(1000);
  digitalWrite(yellowPin, HIGH);
  delay(1000);
  digitalWrite(greenPin, HIGH);
  delay(1000);
  digitalWrite(redPin, LOW);
  delay(200);
  digitalWrite(yellowPin, LOW);
  delay(200);
  digitalWrite(greenPin, LOW);
  delay(200);
}
