#include <Arduino.h>
#include <DS1307.h>
#include <EEPROM.h>

// Init clock object
DS1307 clock;

// Set working address
int wAddr = 100;
char data[100];

int seednum();

void setup() {

	Serial.begin(9600);

	clock.begin();

	// Get random seed
	int seedNum = seednum();
	randomSeed(seedNum);

	// Read current EEPROM addres value
	int oldMemVal = EEPROM.read(wAddr);
	sprintf(data, "Old EEPROM value: %d",oldMemVal);
	Serial.println(data);

	// Get new random number
	int randNum = random(100, 999);
	sprintf(data, "New random number: %d",randNum);
	Serial.println(data);

	// Write new value to EEPROM
	EEPROM.write(wAddr, randNum);

	// Read new EEPROM addres value
	int newMemVal = EEPROM.read(wAddr);
	sprintf(data, "UPDATED EEPROM value: %d",newMemVal);
	Serial.println(data);

	Serial.println("==========================================");
}

void loop() {
}

int seednum() {

	clock.getTime();
	int seedTime = clock.hour + clock.minute + clock.second;
	int seedDate = clock.dayOfMonth + clock.month + clock.year;
	int seednum = (((seedTime * seedDate) + (seedTime / seedDate)) / 2);

	return seednum;
}
