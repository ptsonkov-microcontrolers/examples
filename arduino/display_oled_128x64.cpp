#include <Arduino.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_I2CDevice.h>

#define DISPLAY_ADDR 0x3C
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

void mainScreen();
void drawChart(int page);

void setup() {
	Serial.begin(115200);

	if(!display.begin(SSD1306_SWITCHCAPVCC, DISPLAY_ADDR)) { // Address 0x3D for 128x64
		Serial.println(F("SSD1306 allocation failed"));
		for(;;);
	}
	// display.display();
	// delay(2000);

  mainScreen();
  // drawChart(1);
}

void loop() {

}

void mainScreen() {
  display.clearDisplay();

  display.setTextSize(1);
  display.setTextColor(WHITE);

  // Print vertical display separator
	display.setCursor(60, 0);
  display.println("|");
	display.setCursor(60, 8);
  display.println("|");
	display.setCursor(60, 16);
  display.println("|");
	display.setCursor(60, 24);
  display.println("|");
	display.setCursor(60, 32);
  display.println("|");
	display.setCursor(60, 40);
  display.println("|");
	display.setCursor(60, 48);
  display.println("|");
	display.setCursor(60, 56);
  display.println("|");

  // Print feeding timers
	display.setCursor(0, 5);
	display.println("Feeding:");
	display.setCursor(0, 20);
	display.println("12:03:00");
	display.setCursor(0, 30);
	display.println("13:24:00");
	display.setCursor(0, 40);
	display.println("17:45:00");

  // Print current time
  display.setCursor(70, 5);
  display.println("Now is:");
  display.setCursor(70, 20);
  display.println("22:50:48");

	// Command row
	display.setCursor(70, 50);
	display.print("MENU");
	display.setCursor(110, 50);
	display.write(25);

  display.display();
}

void drawChart(int page) {
  int16_t start, end;

  display.clearDisplay();

  display.setTextSize(1);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  display.cp437(true);

  if (page == 1) {
    start = 0;
    end = 128;
  } else if (page == 2) {
    start = 129;
    end = 256;
  }

  // Not all the characters will fit on the display. This is normal.
  // Library will draw what it can and the rest will be clipped.
  for(int16_t i = start; i < end; i++) {
    if (i == '\n') {
      display.write(' ');
    } else {
      display.write(i);
    };
  }

  display.display();
}
