#include <Arduino.h>

int assignButton(int vDrop);
void testPrint();

void setup() {
  Serial.begin(9600);
  pinMode(A0, INPUT_PULLUP);
}

void loop() {
  testPrint();
  delay(500);
}

int assignButton(int vDrop) {
	int buttton = 0;
	if (vDrop < 75) {
		buttton = 1; // click Back
	} else if (vDrop < 110) {
		buttton = 2; // click Left
	} else if (vDrop < 145) {
		buttton = 3; // click Up
	} else if (vDrop < 175) {
		buttton = 4; // click Down
	} else if (vDrop < 205) {
		buttton = 5; // click Right
	} else if (vDrop < 280) {
		buttton = 6; // click Select
	}
	return buttton;
}

void testPrint() {
	int v = analogRead(0);
	Serial.print("Voltage drop: ");
	Serial.print(v);
	Serial.println();

	int button = assignButton(v);
	Serial.print("button ");
	switch (button) {
	case 0:
		Serial.print(" ------");
		break;
	case 1:
		Serial.print(" (BACK)");
		break;
	case 2:
		Serial.print(" (LEFT)");
		break;
	case 3:
		Serial.print(" (UP)");
		break;
	case 4:
		Serial.print(" (DOWN)");
		break;
	case 5:
		Serial.print(" (RIGHT)");
		break;
	case 6:
		Serial.print(" (SELECT)");
		break;
	}
	Serial.println();
}
