#include <Arduino.h>
#include <HX711.h>

// Pins definition
#define LOADCELL_DOUT_PIN 5
#define LOADCELL_SCK_PIN 6

// Create instances
HX711 loadcell;

void setup()
{
  Serial.begin(115200);
  
  // Set loadcell parameters
  loadcell.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);

  Serial.println("Set scale");
  loadcell.set_scale();
  Serial.println("Tare");
  loadcell.tare();
  Serial.println("Place weight now...");
  delay(5000);
}

void loop()
{
  Serial.print("RAW weight:     ");
  Serial.println(loadcell.get_units(10), 2);
  loadcell.power_down();
  delay(5000);
  loadcell.power_up();
}
