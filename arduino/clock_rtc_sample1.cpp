#include <Arduino.h>
#include <Wire.h>

// MFP => pin A0
// Multi Function Pin

void WriteRTCByte(const unsigned char adr, const unsigned char data);
void DisplayRTCData(const unsigned char adr, const unsigned char validbits);

void setup()
{
   Serial.begin(9600);
   Wire.begin();
}

void loop(){

  WriteRTCByte(0,0);       //STOP RTC
  WriteRTCByte(1,0x18);    //MINUTE=18
  WriteRTCByte(2,0x11);    //HOUR=11
  WriteRTCByte(3,0x09);    //DAY=1(MONDAY) AND VBAT=1
  WriteRTCByte(4,0x28);    //DATE=28
  WriteRTCByte(5,0x02);    //MONTH=2
  WriteRTCByte(6,0x11);    //YEAR=11
  WriteRTCByte(0,0x80);    //START RTC, SECOND=00
  delay(100);

  while(1){
    Serial.print("20");    //year beginning with 20xx
    DisplayRTCData(6,8);
    Serial.print(".");
    DisplayRTCData(5,5);
    Serial.print(".");
    DisplayRTCData(4,6);
    Serial.print(" ");
    DisplayRTCData(2,6);
    Serial.print(":");
    DisplayRTCData(1,7);
    Serial.print(":");
    DisplayRTCData(0,7);
    Serial.println();

    delay(1000);
  }
}

unsigned char ReadRTCByte(const unsigned char adr){
  unsigned char data;

  Wire.beginTransmission(0x6f);
  Wire.write(adr);
  Wire.endTransmission();

  Wire.requestFrom(0x6f,1);
  while (Wire.available()) data=Wire.read();

  return data;
}

void WriteRTCByte(const unsigned char adr, const unsigned char data){
  Wire.beginTransmission(0x6f);
  Wire.write(adr);
  Wire.write(data);
  Wire.endTransmission();
}

void DisplayRTCData(const unsigned char adr, const unsigned char validbits){
  unsigned char data;

  data=ReadRTCByte(adr);
  data=data & 0xff>>(8-validbits);
  if (data<10) Serial.print("0");  //leading zero
  Serial.print(data,HEX);
}
