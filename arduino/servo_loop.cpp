#include <Arduino.h>
#include <Servo.h>

Servo myservo;  // create servo object to control a servo

// int potpin = 0;  // analog pin used to connect the potentiometer
// int val;    // variable to read the value from the analog pin

int rotorPosition;         // variable to store the servo position
int stepDelay = 15;      // Delay between each moving step
int maxAngle = 180;        // Movement angle
int movementStep = 1;       // Rotor movement angle

void setup() {
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object
}

// void loop() {
//   val = analogRead(potpin);            // reads the value of the potentiometer (value between 0 and 1023)
//   val = map(val, 0, 1023, 0, 180);     // scale it to use it with the servo (value between 0 and 180)
//   myservo.write(val);                  // sets the servo position according to the scaled value
//   delay(15);                           // waits for the servo to get there
// }

void loop() {
  for (rotorPosition = 0; rotorPosition <= maxAngle; rotorPosition += movementStep) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo.write(rotorPosition);              // tell servo to go to position in variable 'rotorPosition'
    delay(stepDelay);                       // waits 15ms for the servo to reach the position
  }
  for (rotorPosition = maxAngle; rotorPosition >= 0; rotorPosition -= movementStep) { // goes from 180 degrees to 0 degrees
    myservo.write(rotorPosition);              // tell servo to go to position in variable 'rotorPosition'
    delay(stepDelay);                       // waits 15ms for the servo to reach the position
  }
}
