#include <Arduino.h>
#include <LiquidCrystal.h>
/*
   LCD circuit setup:
   * LCD pin1  (VSS)     -  Arduino GND
   * LCD pin2  (VCC)     -  Arduino 5V
   * LCD pin3  (Vo)      -  Potentiometer wiper (display contrast control)
   * LCD pin4  (RS)      -  Arduino Digital-12
   * LCD pin5  (R/W)     -  Arduino GND
   * LCD pin6  (E)       -  Arduino Digital-11
   * LCD pin11 (DB4)     -  Arduino Digital-5
   * LCD pin12 (DB5)     -  Arduino Digital-4
   * LCD pin13 (DB6)     -  Arduino Digital-3
   * LCD pin14 (DB7)     -  Arduino Digital-2
   * LCD pin15 (A)       -  Display LED backlight + (5V)
   * LCD pin16 (K)       -  Display LED backlight - (GND)
   * Potentiometer ends - to 5V and GND
*/

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

void setup() {
  // set up the LCD's number of columns and rows:
  lcd.begin(20, 4);
  // Print a message to the LCD.
  lcd.setCursor(1, 1);
  lcd.print(" Helo, Rache! :*");
}

void loop() {
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 2);
  // print the number of seconds since reset:
  lcd.print(millis() / 1000);
}
