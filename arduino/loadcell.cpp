#include <Arduino.h>
#include <HX711.h>

// Pins definition
#define LOADCELL_DOUT_PIN 5
#define LOADCELL_SCK_PIN 6

// Create instances
HX711 loadcell;

bool calibrate = false;

void setup()
{
  Serial.begin(115200);
  
  // Set loadcell parameters
  loadcell.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);

  if (calibrate)
  {
    Serial.println("Set scale");
    loadcell.set_scale();
    Serial.println("Tare");
    loadcell.tare();
    Serial.println("Place weight now...");
    delay(5000);
    Serial.print("RAW weight:     ");
    Serial.println(loadcell.get_units(10), 2);
  }
  else
  {
    Serial.println("Before setting up the scale:");
    // print a raw reading from the ADC
    Serial.print("read:             ");
    Serial.println(loadcell.read());
    // print the average of 20 readings from the ADC
    Serial.print("read average:     ");
    Serial.println(loadcell.read_average(20));
    // print the average of 5 readings from the ADC minus the tare weight (not set yet)
    Serial.print("get value:        ");
    Serial.println(loadcell.get_value(5));
    // print the average of 5 readings from the ADC minus tare weight (not set) divided
    // by the SCALE parameter (not set yet)
    Serial.print("get units:        ");
    Serial.println(loadcell.get_units(5), 1);
    // this value is obtained by calibrating the scale with known weights
    // How to calibrate your load cell (https://github.com/bogde/HX711)
    // 1. Call set_scale() with no parameter.
    // 2. Call tare() with no parameter.
    // 3. Place a known weight on the scale and call get_units(10).
    // 4. Divide the result in step 3 to your known weight. You should get about the parameter you need to pass to set_scale().
    // 5. Adjust the parameter in step 4 until you get an accurate reading.
    loadcell.set_scale(2085.f);
    // reset the scale to 0
    loadcell.tare();

    Serial.println("After setting up the scale:");
    // print a raw reading from the ADC
    Serial.print("read:             ");
    Serial.println(loadcell.read());
    // print the average of 20 readings from the ADC
    Serial.print("read average:     ");
    Serial.println(loadcell.read_average(20));
    // print the average of 5 readings from the ADC minus the tare weight, set with tare()
    Serial.print("get value:        ");
    Serial.println(loadcell.get_value(5));
    // print the average of 5 readings from the ADC minus tare weight, divided
    // by the SCALE parameter set with set_scale
    Serial.print("get units:        ");
    Serial.println(loadcell.get_units(5), 1);

    Serial.println("Readings:");
  }
}

void loop()
{
  if (calibrate)
  {
    Serial.print("RAW weight:     ");
    Serial.println(loadcell.get_units(10), 2);
    loadcell.power_down();
    delay(5000);
    loadcell.power_up();
  }
  else
  {
    Serial.print("one reading:   ");
    Serial.print(loadcell.get_units(), 1);
    Serial.print("\t| average:   ");
    Serial.println(loadcell.get_units(10), 1);
    // put the ADC in sleep mode
    loadcell.power_down();
    delay(5000);
    loadcell.power_up();
  }
}
