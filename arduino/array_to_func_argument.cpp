#include <Arduino.h>

int oneDimension[4] = {100, 101, 102, 103};

int twoDimension[4][3] = {
	{0, 0, 0},
	{1, 11, 12},
	{2, 21, 22},
	{3, 31, 33}
};

void readOneDimension(int array[4]);
void readTwoDimension(int array[4][3]);

void setup() {

	Serial.begin(9600);

	Serial.println();
	Serial.println("Print one dimensional array");
	readOneDimension(oneDimension);

	Serial.println();
	Serial.println("Print two dimensional array");
	readTwoDimension(twoDimension);

}

void loop() {
}

void readOneDimension(int array[4]) {
	for(int i = 0; i < 4; i++) {
		Serial.println(array[i]);
	}
}

void readTwoDimension(int array[4][3]) {
	for(int i = 0; i < 4; i++) {
		for(int y = 0; y < 3; y++) {
			Serial.print(array[i][y]);
			Serial.print(" ");
		}
		Serial.println();
	}
}
